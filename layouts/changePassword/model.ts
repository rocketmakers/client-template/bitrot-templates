/**
 * Specify required object
 *
 * @examples require(".").sampleData
 */
export interface IModel {
  username: string;
  requestDateTime: string;
}

export const sampleData: IModel[] = [
  {
    username: 'test@rocketmakers.com',
    requestDateTime: '10am 12th March',
  },
  {
    username: 'test2@rocketmakers.com',
    requestDateTime: '10am 12th March  ',
  },
];
